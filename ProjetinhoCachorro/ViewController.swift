//
//  ViewController.swift
//  ProjetinhoCachorro
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cachorro: Decodable {
    let message: String
    let status: String
}

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoCachorro()
        loadImage()
    }

    @IBAction func recarregarImagem(_ sender: Any) {
        getNovoCachorro()
        loadImage()
    }
    
    func getNovoCachorro(){
        AF.request("https://dog.ceo/api/breeds/image/random").responseDecodable(of: Cachorro.self){
            response in
            if let cachorro = response.value{
                self.imageView.kf.setImage(with: URL(string: cachorro.message))
            }
        }
    }
    
    func loadImage(){
        activityIndicator.startAnimating()
        
    }
    
}

